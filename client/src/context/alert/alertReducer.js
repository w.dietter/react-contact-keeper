import { AlertActionTypes } from '../types';

export default (state, action) => {
	switch (action.type) {
		case AlertActionTypes.SET_ALERT:
			return [...state, action.payload];

		case AlertActionTypes.REMOVE_ALERT:
			return state.filter(alert => alert.id !== action.payload);
		default:
			return state;
	}
};
