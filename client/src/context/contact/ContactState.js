import React, { useReducer } from 'react';
import ContactContext from './contactContext';
import contactReducer from './contactReducer';
import { ContactActionTypes } from '../types';
import axios from 'axios';

const ContactState = props => {
	const initialState = {
		contacts: null,
		current: null,
		filtered: null,
		error: null
	};

	const [state, dispatch] = useReducer(contactReducer, initialState);

	/* get contacts */
	const getContacts = async () => {
		try {
			const response = await axios.get('/api/contacts');
			dispatch({
				type: ContactActionTypes.GET_CONTACTS,
				payload: response.data
			});
		} catch (error) {
			dispatch({
				type: ContactActionTypes.CONTACT_ERROR,
				payload: error.response.msg
			});
		}
	};

	/* add contact */
	const addContact = async contact => {
		const config = {
			headers: {
				'Content-Type': 'application/json'
			}
		};

		try {
			const response = await axios.post('/api/contacts', contact, config);
			dispatch({
				type: ContactActionTypes.ADD_CONTACT,
				payload: response.data
			});
		} catch (error) {
			dispatch({
				type: ContactActionTypes.CONTACT_ERROR,
				payload: error.response.msg
			});
		}
	};

	/* update contact */
	const updateContact = async contact => {
		const config = {
			headers: {
				'Content-Type': 'application/json'
			}
		};

		try {
			const response = await axios.put(
				`/api/contacts/${contact._id}`,
				contact,
				config
			);
			dispatch({ type: ContactActionTypes.UPDATE_CONTACT, payload: contact });
		} catch (error) {
			dispatch({
				type: ContactActionTypes.CONTACT_ERROR,
				payload: error.response.msg
			});
		}
	};

	/* delete contact */
	const deleteContact = async id => {
		try {
			const response = await axios.delete(`/api/contacts/${id}`);
			dispatch({ type: ContactActionTypes.DELETE_CONTACT, payload: id });
		} catch (error) {
			dispatch({
				type: ContactActionTypes.CONTACT_ERROR,
				payload: error.response.msg
			});
		}
	};

	/* set current contact */
	const setCurrent = contact => {
		dispatch({ type: ContactActionTypes.SET_CURRENT, payload: contact });
	};
	/* clear current contact */
	const clearCurrent = () => {
		dispatch({ type: ContactActionTypes.CLEAR_CURRENT });
	};

	/* filter contacts */
	const filterContacts = text => {
		dispatch({ type: ContactActionTypes.FILTER_CONTACTS, payload: text });
	};
	/* clear filter */
	const clearFilter = () => {
		dispatch({ type: ContactActionTypes.CLEAR_FILTER });
	};

	const clearContacts = () => {
		dispatch({ type: ContactActionTypes.CLEAR_CONTACTS });
	};

	return (
		<ContactContext.Provider
			value={{
				contacts: state.contacts,
				current: state.current,
				filtered: state.filtered,
				error: state.error,
				addContact,
				deleteContact,
				setCurrent,
				clearCurrent,
				updateContact,
				filterContacts,
				clearFilter,
				getContacts,
				clearContacts
			}}
		>
			{props.children}
		</ContactContext.Provider>
	);
};

export default ContactState;
