import { ContactActionTypes } from '../types';

export default (state, action) => {
	switch (action.type) {
		case ContactActionTypes.GET_CONTACTS:
			return {
				...state,
				loading: false,
				contacts: action.payload
			};
		case ContactActionTypes.ADD_CONTACT:
			return {
				...state,
				loading: false,
				contacts: [action.payload, ...state.contacts]
			};
		case ContactActionTypes.CONTACT_ERROR:
			return {
				...state,
				loading: false,
				error: action.payload
			};
		case ContactActionTypes.DELETE_CONTACT:
			return {
				...state,
				loading: false,
				contacts: state.contacts.filter(
					contact => contact._id !== action.payload
				)
			};
		case ContactActionTypes.UPDATE_CONTACT:
			return {
				...state,
				loading: false,
				contacts: state.contacts.map(contact =>
					contact._id === action.payload._id ? action.payload : contact
				)
			};
		case ContactActionTypes.SET_CURRENT:
			return {
				...state,
				current: action.payload
			};
		case ContactActionTypes.CLEAR_CURRENT:
			return {
				...state,
				current: null
			};
		case ContactActionTypes.FILTER_CONTACTS:
			return {
				...state,
				loading: false,
				filtered: state.contacts.filter(contact => {
					const regex = new RegExp(`${action.payload}`, 'gi');
					return contact.name.match(regex) || contact.email.match(regex);
				})
			};
		case ContactActionTypes.CLEAR_CONTACTS:
			return {
				...state,
				contacts: null,
				filtered: null,
				error: null,
				current: null
			};

		case ContactActionTypes.CLEAR_FILTER:
			return {
				...state,
				filtered: null
			};
		default:
			return state;
	}
};
