import { AuthActionTypes } from '../types';

export default (state, action) => {
	switch (action.type) {
		case AuthActionTypes.USER_LOADED:
			return {
				...state,
				isAuthenticated: true,
				loading: false,
				user: action.payload
			};
		case AuthActionTypes.REGISTER_FAIL:
		case AuthActionTypes.AUTH_ERROR:
		case AuthActionTypes.LOGIN_FAIL:
		case AuthActionTypes.LOGOUT:
			localStorage.removeItem('token');
			return {
				...state,
				token: null,
				isAuthenticated: false,
				loading: false,
				user: null,
				error: action.payload
			};
		case AuthActionTypes.REGISTER_SUCCESS:
		case AuthActionTypes.LOGIN_SUCCESS:
			localStorage.setItem('token', action.payload.token);
			return {
				...state,
				...action.payload,
				isAuthenticated: true,
				loading: false
			};
		case AuthActionTypes.CLEAR_ERRORS:
			return {
				...state,
				error: null
			};

		default:
			return state;
	}
};
