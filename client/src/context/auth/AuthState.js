import React, { useReducer } from 'react';
import AuthContext from './authContext';
import authReducer from './authReducer';
import { AuthActionTypes } from '../types';
import axios from 'axios';
import setAuthToken from '../../utils/setAuthToken';

const AuthState = props => {
	const initialState = {
		toke: localStorage.getItem('token'),
		isAuthenticated: null,
		loading: true,
		user: null,
		error: null
	};

	const [state, dispatch] = useReducer(authReducer, initialState);

	/* Load user */
	const loadUser = async () => {
		//@todo - load token into global headers
		if (localStorage.token) {
			setAuthToken(localStorage.token);
		}
		try {
			const response = await axios.get('/api/auth');
			/* response -> user data */
			dispatch({ type: AuthActionTypes.USER_LOADED, payload: response.data });
		} catch (error) {
			dispatch({ type: AuthActionTypes.AUTH_ERROR });
		}
	};
	/* Register user */
	const registerUser = async formData => {
		const config = {
			headers: {
				'Content-Type': 'application/json'
			}
		};

		try {
			const response = await axios.post('/api/users', formData, config);
			dispatch({
				type: AuthActionTypes.REGISTER_SUCCESS,
				payload: response.data
			});
			loadUser();
		} catch (error) {
			dispatch({
				type: AuthActionTypes.REGISTER_FAIL,
				payload: error.response.data.msg
			});
		}
	};
	/* Login user */
	const loginUser = async formData => {
		const config = {
			headers: {
				'Content-Type': 'application/json'
			}
		};

		try {
			const response = await axios.post('/api/auth', formData, config);
			dispatch({ type: AuthActionTypes.LOGIN_SUCCESS, payload: response.data });
			loadUser();
		} catch (error) {
			dispatch({
				type: AuthActionTypes.LOGIN_FAIL,
				payload: error.response.data.msg
			});
		}
	};

	/* Logout */
	const logoutUser = () => {
		dispatch({ type: AuthActionTypes.LOGOUT });
	};

	/* Clear errors */
	const clearErrors = () => dispatch({ type: AuthActionTypes.CLEAR_ERRORS });

	return (
		<AuthContext.Provider
			value={{
				token: state.token,
				isAuthenticated: state.isAuthenticated,
				loading: state.loading,
				error: state.error,
				user: state.user,
				registerUser,
				loadUser,
				loginUser,
				logoutUser,
				clearErrors
			}}
		>
			{props.children}
		</AuthContext.Provider>
	);
};

export default AuthState;
