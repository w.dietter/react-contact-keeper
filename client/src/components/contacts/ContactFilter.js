import React, { useContext, useRef, useEffect } from 'react';
/* context */
import ContactContext from '../../context/contact/contactContext';

const ContactFilter = () => {
	const contactContext = useContext(ContactContext);

	const { filterContacts, clearFilter, filtered } = contactContext;

	const text = useRef('');

	useEffect(() => {
		if (filtered === null) {
			text.current.value = '';
		}
	}, [filtered]);

	const onChange = ({ target }) => {
		if (text.current.value !== '') {
			filterContacts(target.value);
		} else {
			clearFilter();
		}
	};

	return (
		<form>
			<input
				type="text"
				name="text"
				ref={text}
				placeholder="filter contacts..."
				onChange={onChange}
			/>
		</form>
	);
};

export default ContactFilter;
