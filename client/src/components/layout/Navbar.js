import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
/* components */
import { FaIdCardAlt, FaSignOutAlt } from 'react-icons/fa';
/* context */
import AuthContext from '../../context/auth/authContext';
import ContactContext from '../../context/contact/contactContext';

const Navbar = ({ title }) => {
	const authContext = useContext(AuthContext);
	const { isAuthenticated, logoutUser, user } = authContext;
	const contactContext = useContext(ContactContext);
	const { clearContacts } = contactContext;

	const onLogout = () => {
		logoutUser();
		clearContacts();
	};

	const authLinks = (
		<>
			<li>Hello {user ? user.user.name : null}</li>
			<li>
				<a href="#!" onClick={onLogout}>
					<FaSignOutAlt /> <span className="hide-sm">Logout</span>
				</a>
			</li>
		</>
	);

	const guestLinks = (
		<>
			<li>
				<Link to="/register">Register</Link>
			</li>
			<li>
				<Link to="/login">Login</Link>
			</li>
		</>
	);

	return (
		<div className="navbar bg-primary">
			<h1>
				<FaIdCardAlt />
				&nbsp;
				{title}
			</h1>
			<ul>
				{/* <li>
					<Link to="/">Home</Link>
				</li>
				<li>
					<Link to="/about">About</Link>
				</li> */}
				{isAuthenticated ? authLinks : guestLinks}
			</ul>
		</div>
	);
};

Navbar.defaultProps = {
	title: 'Contact Keeper'
};

Navbar.propTypes = {
	title: PropTypes.string.isRequired
};

export default Navbar;
