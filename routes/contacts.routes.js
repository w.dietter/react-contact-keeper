const express = require('express');
const router = express.Router();
const { check, validationResult } = require('express-validator/check');
const Contact = require('../models/Contact.model');
const protectedRoute = require('../middleware/auth.middleware');
/*
  @route 			GET			api/contacts
  @desc 			Get all users contacts
  @access 		Private        
*/
router.get('/', protectedRoute, async (req, res) => {
	try {
		const contacts = await Contact.find({ user: req.user.id }).sort({
			date: -1
		});
		return res.json(contacts);
	} catch (error) {
		return res.status(500).send('Server error');
	}
});

/*
  @route 			POST			api/contacts
  @desc 			Add contact
  @access 		Private        
*/
router.post(
	'/',
	[
		protectedRoute,
		[
			check('name', 'Name is required')
				.not()
				.isEmpty()
		]
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		const { name, email, phone, type } = req.body;

		try {
			const newContact = new Contact({
				name,
				email,
				phone,
				type,
				user: req.user.id
			});

			const contact = await newContact.save();
			return res.json(contact);
		} catch (error) {
			console.error(error.message);
			return res.status(500).send('Server error');
		}
	}
);

/*
  @route 			PUT			api/contacts/:id
  @desc 			Update a single contact
  @access 		Private        
*/
router.put(
	'/:id',
	[
		protectedRoute,
		[
			check('name', 'Name is required')
				.not()
				.isEmpty()
		]
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		const { id } = req.params;
		const { name, email, phone, type } = req.body;

		try {
			const updatedContact = await Contact.findByIdAndUpdate(
				{ _id: id },
				{ name, email, phone, type },
				{ new: true }
			);
			console.log(updatedContact);
			return res.json(updatedContact);
		} catch (error) {
			console.error(error.message);
			return res.status(500).send('Server Error');
		}
	}
);

/*
  @route 			DELETE			api/contacts/:id
  @desc 			Delete a contact
  @access 		Private
*/
router.delete('/:id', protectedRoute, async (req, res) => {
	const { id } = req.params;
	try {
		await Contact.findByIdAndDelete({ _id: id });
		return res.json({ msg: 'Succesfully deleted' });
	} catch (error) {
		console.log(error);
		return res.status(500).send('Server Error');
	}
});

module.exports = router;
