const express = require('express');
const app = express();
const connectDB = require('./config/db');

/* Connect Database */
connectDB();

const PORT = process.env.PORT || 5000;

/* init middleware */
app.use(express.json({ extended: false }));

/* Define Routes */
app.use('/api/users', require('./routes/users.routes'));
app.use('/api/contacts', require('./routes/contacts.routes'));
app.use('/api/auth', require('./routes/auth.routes'));

app.listen(PORT, () => console.log('server listening on PORT ' + PORT));
